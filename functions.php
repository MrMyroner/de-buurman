<?php

// ??? 
//include 'settings.php';

is_file($f = __DIR__ . '/vendor/autoload.php') && require $f;


if (!class_exists('Timber')) {

    if (!class_exists('Timber\Timber')) {
        add_action('admin_notices', function () {
            echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' .
                 esc_url(admin_url('plugins.php#timber')) .
                 '">' .
                 esc_url(admin_url('plugins.php')) .
                 '</a></p></div>';
        });

        add_filter('template_include', function () {
            return get_stylesheet_directory() . '/static/no-timber.html';
        });

        return;
    }

    // This is to circumvent intellisense inspection.
    // class_alias is not detected.
    class Timber extends Timber\Timber
    {
    }

    class TimberSite extends Timber\Site
    {
    }

    class TimberMenu extends Timber\Menu
    {
    }

    class TimberMenuItem extends Timber\MenuItem
    {
    }

    class TimberUser extends Timber\User
    {
    }

    class TimberPost extends Timber\Post
    {
    }

    class TimberImage extends Timber\Image
    {
    }
}
define('TIMBER_LOC', __DIR__ . '/../../timber/de-buurman');
Timber::$dirname = array('templates', 'views');
Timber::$cache   = true;

class StarterSite extends TimberSite
{

    function __construct()
    {

        add_theme_support('post-formats');
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
        add_theme_support('custom-header');
        add_theme_support('custom-logo');

        add_filter('timber_context', array($this, 'add_to_context'));
        add_filter('get_twig', array($this, 'add_to_twig'));

        add_filter('wpcf7_form_elements', function ($html) {
//            echo $html; die;
//            var_dump(func_get_args());
//            die;
            return str_replace(array('has-error', 'help-block'), array('has-danger', 'form-text'), $html);
        }, 99, 99);

        add_action('init', array($this, 'register_post_types'));
        add_action('init', array($this, 'register_taxonomies'));
        if(!WP_DEBUG) {
            add_action('after_setup_theme', array($this, 'remove_admin_bar_user'));
        }


        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
        add_filter('post_gallery', array($this, 'customFormatGallery'), 10, 2);

        parent::__construct();
    }

    function customFormatGallery($string, $attr)
    {

        $output = "<div class=\"row\">";
        $posts  = get_posts(array('include' => $attr['ids'], 'post_type' => 'attachment'));
        $id     = uniqid();
        foreach ($posts as $imagePost) {
            //var_dump($imagePost);
            $output .= "<div class='col-xs-12 col-sm-6 col-lg-3 gallery-img-holder' >";
            $output .= '<a href="' .
                       wp_get_attachment_image_src($imagePost->ID, 'large')[0] .
                       '" rel="gal_' .
                       $id .
                       '" class="fancybox" >';
            $output .= "<img src='" .
                       wp_get_attachment_image_src($imagePost->ID, 'thumbnail')[0] .
                       "' class='gallery-img' title='" .
                       $imagePost->post_excerpt .
                       "'  />";
            $output .= '</a>';
            $output .= '</div>';

        }

        $output .= "</div>";

        return $output;
    }

    function register_post_types()
    {
        //this is where you can register custom post types
    }

    function enqueue_scripts()
    {
        if (is_front_page()) {
            wp_enqueue_script('bxslider',
                get_stylesheet_directory_uri() . ($f = '/bower_components/bxslider-4/dist/jquery.bxslider.min.js'),
                array('jquery'), filemtime(__DIR__ . $f), true);
            wp_enqueue_style('bxslider',
                get_stylesheet_directory_uri() . ($f = '/bower_components/bxslider-4/dist/jquery.bxslider.min.css'),
                array(), filemtime(__DIR__ . $f));
            wp_enqueue_script('frontpage-slideshow', get_stylesheet_directory_uri() . ($f = '/static/frontpage.js'),
                array('jquery'), filemtime(__DIR__ . $f), true);
        }

    }

    function register_taxonomies()
    {
        //this is where you can register custom taxonomies
    }

    function remove_admin_bar_user()
    {
        if (current_user_can('administrator') || is_admin()) {
//            show_admin_bar(false);
        }
    }

    function add_to_context($context)
    {
        $context['menu'] = new TimberMenu();
        $context['site'] = $this;

        global $wp_query;
        $context['site_width'] = $this->site_width;

        $context['wp_query'] = $wp_query;

//		$context['mainmenu'] = new T\Menu( 'mainmenu' );
        foreach (get_nav_menu_locations() as $location => $menu_id) {
            $context['menus'][$location] = new Timber\Menu($menu_id);
        }

        return $context;
    }

    function add_to_twig(Twig_Environment $twig)
    {
        if (WP_DEBUG) {
            $twig->enableAutoReload();
            $twig->enableStrictVariables();
            $twig->enableDebug();
        } elseif (current_user_can('install_themes')) {
            $twig->enableAutoReload();
            $twig->disableStrictVariables();
            $twig->disableDebug();
        } else {
            $twig->disableAutoReload();
            $twig->disableStrictVariables();
            $twig->disableDebug();
        }

        $twig->addGlobal('site', $this);
        $twig->addGlobal('pixel', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7');

        $twig->addFilter(new Twig_SimpleFilter('srcset', $srcset = function ($id, $size = null, $meta = null) {
            if ($id instanceof TimberImage) {
                $id = $id->id;
            }
            if (is_string($size) && preg_match('@^\d+x\d+$@', $size)) {
                $size = explode('x', $size);
            }

            return wp_get_attachment_image_srcset($id, $size, $meta);
        }));
        $twig->addFunction(new Twig_SimpleFunction('srcset', $srcset));

        // the buttons, they do nothing!!
        $twig->registerUndefinedFunctionCallback(function ($name) {
            if ($name == 'dump') {
                return new Twig_SimpleFunction('dump', function () {
                });
            }

            return null;
        });

        return $twig;
    }

}

new StarterSite();
