<?php
/*
 * Template Name: Home template
 * Description: Voorkant
 */

$context = Timber::get_context();
$post = new TimberPost();


$context['post'] = $post;

$context['cover_image'] = isset($post->custom['image_top']) ? new TimberImage ($post->custom['image_top']) : false;

$group_ID = 283;

$fields = array();
$fields = apply_filters('acf/field_group/get_fields', $fields, $group_ID);
$i = 0;
$slider = array();

foreach($fields as $field) {
    $slideimage = get_field($field['name']);
    array_push($slider, $slideimage);
    $context['slide_images'] = $slider;
    }

$pics = array();
$this_img = array();
for ($i = 0; $i <= 6; $i++) {

    $this_img['title'] = get_field("feature_" . $i . "_title");
    $this_img['text'] = get_field("feature_" . $i . "_text");
    $this_img['pic'] = get_field("feature_" . $i . "_pic");
    $this_img['pic'] = $this_img['pic']['url'];
    $this_img['link'] = get_field("feature_" . $i . "_link");

    if (!empty($this_img['title']) && !empty($this_img['pic'])) {
        $pics[] = $this_img;
    }
}

$context['images'] = $pics;

Timber::render('page-home.twig', $context);