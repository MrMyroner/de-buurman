<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}
$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
$context['foo'] = 'bar';
$templates = array( 'index.twig' );
if ( is_home() ) {

    $pics = array();

    for ($i = 0; $i <= 7; $i++) {

        $this_url = array();
        $this_url['link'] = get_field('feature_' . $i . 'link');

        if (!empty($this_url['link'])) {
            $pics[] = $this_url;
        }
        $context['images'] = $pics;

        array_unshift($templates, 'home.twig');
    }

}
Timber::render('page-pic.twig', $context);
Timber::render( $templates, $context );
