<?php
/*
 * Template Name: menucard template
 * Description: Voorkant
 */

$post = new TimberPost();

$context['post'] = $post;

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();

$context['menukaarten'] = \Timber\TermGetter::get_terms(array(
    'taxonomy' => 'menukaart',
    'parent'   => 0,
));

$context['subcats'] = \Timber\TermGetter::get_terms(array(
    'taxonomy' => 'menukaart',
));

$context['baseurl'] = site_url();

$context['images'] = get_field('foto_2');

Timber::render(array('page-menucard.twig', 'archive.twig'), $context);