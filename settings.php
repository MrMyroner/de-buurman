<?php
/**
 * Created by PhpStorm.
 * User: Jarrin
 * Date: 20-9-2016
 * Time: 11:09
 */
add_action('admin_menu', 'add_page');

if (!function_exists('add_page')) {
    //function to add page under setting options in wordpress admin section
    function add_page()
    {
        add_options_page('De Buurman', 'De Buurman', 'manage_options', 'buurman', 'buurman_options_frontpage');
    }
}

function buurman_options_frontpage()
{
    ?>
    <div class="wrap">
        <h2>De Buurman extra instellingen </h2>
        <form action="options.php" method="post">
            <?php settings_fields('buurman_options'); ?>
            <?php do_settings_sections('buurman'); ?>
            <table class="form-table">
                <tr valign="top">
                    <td colspan="2">
                        <input name="Submit" type="submit" class="button button-primary"
                               value="<?php esc_attr_e('Save Changes'); ?>"/>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <?php
}

add_action('admin_init', 'buurman_admin_init');

function buurman_admin_init()
{
    register_setting('buurman_options', 'buurman_options', 'buurman_options_validate');
    add_settings_section('buurman_main', 'Main Settings', 'buurman_section_text', 'buurman');
    add_settings_field('buurman_text_input1', 'Input 1', 'buurman_input1', 'buurman', 'buurman_main');
    add_settings_field('buurman_text_input2', 'Input 2', 'buurman_input2', 'buurman', 'buurman_main');

    add_settings_field('myprefix_setting-id',
        'This is the setting title',
        'myprefix_setting_callback_function',
        'buurman',
        'buurman_main',
        array('label_for' => 'myprefix_setting-id'));

}

function buurman_section_text()
{

}

function buurman_input1()
{

}

function buurman_input2()
{

}

function myprefix_setting_callback_function()
{

}


add_action('wp_loaded', function () {

    global $wp_post_types;

    if (isset($wp_post_types['menu'])) {
        /** @var  WP_Post_Type $menu_cpt */
        $menu_cpt = $wp_post_types['menu'];
        /** @var \stdClass $menu_labels */
        $menu_labels = $menu_cpt->labels;

//        $menu_labels->
//        var_dump($menu_labels);
//        die;
    }

}, 20);
