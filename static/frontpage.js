jQuery(function ($) {
    $('#frontpage-slider').bxSlider({
        onSliderLoad: function () {
            $('.top-image').css({ 'visibility' : 'visible'});
        },
        touchEnabled: false
    });
});