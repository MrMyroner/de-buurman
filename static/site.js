(function ($) {

    $(function(){
        $('.white-box .text').fitText(2.8, {
            maxFontSize: 28
        });

        $('.footer .footer-column').matchHeight();
        $('.gallery-img-holder').matchHeight();
        (function($){
            $('div.wpcf7 > form').submit(function(){
                // The name of a single field you're interested in targeting

                $(this).find('.form-group').bind('DOMSubtreeModified', function(event) {

                    // If an error has been appended to this input's parent span, do something
                    if ( $(this).hasClass('has-error')) {
                        $(this).removeClass('has-error').addClass('has-danger');
                        $(this).find('input,textarea').addClass('form-control-danger');

                        $(this).find('.wpcf7-not-valid-tip').addClass('text-danger');
                        // Prevent this function from running multiple times
                        $(this).off(event);
                    }
                });
            });
        })(jQuery);
    });

})(jQuery);