<?php

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
$context['menukaart'] = $menukaart = new Timber\Term();

$context['baseurl'] = site_url();
$context['menukaarten'] = \Timber\TermGetter::get_terms(array(
    'taxonomy' => 'menukaart',
    'parent'   => 0,
));

$context['subcats'] = \Timber\TermGetter::get_terms(array(
    'taxonomy' => 'menukaart',
));
$term = new \Timber\Term();
$context['menuitems'] = \Timber\PostGetter::get_posts(array(
    'post_type' => 'menuitem',
    'tax_query' => array(
        array(
            'taxonomy' => 'menukaart', //or tag or custom taxonomy
            'field' => 'id',
            'terms' => array($term->id)
        )
    )
));


Timber::render(array('menukaart.twig', 'archive.twig'), $context);